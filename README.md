# One-Touch Switching Client

## Installation
```
npm install @youfibre/otsc
```

## Basic Usage
```typescript
// Add these imports...
import { OTSClient } from '@youfibre/otsc';

const client = new OTSClient(
  'https://hub-base-url',
  'https://sit-base-url',
  'my-client-id',
  'my-client-secret',
);

const clientWithCaching = new OTSClient(
  'https://hub-base-url',
  'https://sit-base-url',
  'my-client-id',
  'my-client-secret',
  {
    setItem: (key: string, value: string, ttlSeconds: number) => {
      // redisClient.set(key, value, 'EX', ttlSeconds);
    },
    getItem: (key: string) => {
      // return redisClient.get(key);
    }
  }
);

const message = {
  envelope: {
    source: {
      type: 'RCPID',
      identity: 'RCP001',
      correlationID: 'XYZ987'
    },
    destination: {
      type: 'RCPID',
      identity: 'RCP002',
      correlationID: 'XYZ123'
    },
    routingID: 'residentialSwitchMatchRequest'
  },

  residentialSwitchMatchRequest: {
    grcpBrandName: 'YouFibre Limited',
    name: 'Miggins', // Dummy data
    account: '0003316563216', // Dummy data
    address: {
      uprn: '12345', // Dummy data
      addressLines: ['Flat 1', 'Rose Cottage', '22 Cheshunt Mews', 'Cypress Street', 'Tyre Industrial Estate', 'Blnatyre'], // Dummy data
      postTown: 'Glasgow', // Dummy data
      postCode: 'SW1P 3UX' // Dummy data
    },
    services: [
      {
        serviceType: 'IAS',
        action: 'cease'
      },
      {
        serviceType: 'NBICS',
        serviceIdentifierType: 'DN',
        serviceIdentifier: '0101111222', // Dummy data
        action: 'port'
      }
    ]
  }
} as SwitchMessage;

const response = await client.postMessage(message);
console.log(response);
```
