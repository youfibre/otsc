export interface EnvelopeAddressee {
  type: 'RCPID';
  identity: string;
  correlationID: string;
}