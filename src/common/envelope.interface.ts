import { EnvelopeAddressee } from "./envelope-addressee.interface";

export interface Envelope {
  source: EnvelopeAddressee;
  destination: EnvelopeAddressee;
  routingID: string;
  auditData?: { name: string; value: string; }[];
}