import { ResidentialSwitchMatchRequest } from "src/gaining-provider/residential-switch-match-request.interface";
import { Envelope } from "./envelope.interface";
import { ResidentialSwitchOrderCancellationRequest } from "src/gaining-provider/residential-switch-order-cancellation-request.interface";
import { ResidentialSwitchOrderTriggerRequest } from "src/gaining-provider/residential-switch-order-trigger-request.interface";
import { ResidentialSwitchOrderUpdateRequest } from "src/gaining-provider/residential-switch-order-update-request.interface";
import { ResidentialSwitchOrderRequest } from "src/gaining-provider/residential-switch-order-request.interface";
import { ResidentialSwitchMatchConfirmation } from "src/losing-provider/residential-switch-match-confirmation.interface";
import { ResidentialSwitchMatchFailure } from "src/losing-provider/residential-switch-match-failure.interface";
import { ResidentialSwitchOrderCancellationConfirmation } from "src/losing-provider/residential-switch-order-cancellation-confirmation.interface";
import { ResidentialSwitchOrderCancellationFailure } from "src/losing-provider/residential-switch-order-cancellation-failure.interface";
import { ResidentialSwitchOrderConfirmation } from "src/losing-provider/residential-switch-order-confirmation.interface";
import { ResidentialSwitchOrderFailure } from "src/losing-provider/residential-switch-order-failure.interface";
import { ResidentialSwitchOrderTriggerConfirmation } from "src/losing-provider/residential-switch-order-trigger-confirmation.interface";
import { ResidentialSwitchOrderTriggerFailure } from "src/losing-provider/residential-switch-order-trigger-failure.interface";
import { ResidentialSwitchOrderUpdateConfirmation } from "src/losing-provider/residential-switch-order-update-confirmation.interface";
import { ResidentialSwitchOrderUpdateFailure } from "src/losing-provider/residential-switch-order-update-failure.interface";
import { MessageDeliveryFailure } from "src/post-office/message-delivery-failure.interface";

interface SwitchMessageBase {
  envelope: Envelope
};

export type SwitchMessage = SwitchMessageBase & (
  ResidentialSwitchMatchRequest |
  ResidentialSwitchOrderCancellationRequest |
  ResidentialSwitchOrderTriggerRequest |
  ResidentialSwitchOrderUpdateRequest |
  ResidentialSwitchOrderRequest |
  ResidentialSwitchMatchConfirmation |
  ResidentialSwitchMatchFailure |
  ResidentialSwitchOrderCancellationConfirmation |
  ResidentialSwitchOrderCancellationFailure |
  ResidentialSwitchOrderConfirmation |
  ResidentialSwitchOrderFailure |
  ResidentialSwitchOrderTriggerConfirmation |
  ResidentialSwitchOrderTriggerFailure |
  ResidentialSwitchOrderUpdateConfirmation |
  ResidentialSwitchOrderUpdateFailure |
  MessageDeliveryFailure
);
