export interface Address {
  uprn?: string;
  addressLines: string[];
  postTown: string;
  postCode: string;
}