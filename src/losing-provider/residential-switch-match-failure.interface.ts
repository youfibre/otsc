import { Address } from "../common/address.interface";
import { SwitchMessage } from "../common/switch-message.interface";

interface FailureCommonElements {
  faultElement?: string;
  faultElementValue?: string;
  address?: Address;
}

interface Failure_AddressNotFound extends FailureCommonElements {
  faultCode: '101',
  faultText: 'Address not found'
}

interface Failure_MissingOrIncompleteAddress extends FailureCommonElements {
  faultCode: '102',
  faultText: 'Missing, or incomplete address'
}

interface Failure_NameNotProvided extends FailureCommonElements {
  faultCode: '103',
  faultText: 'Name not provided'
}

interface Failure_AccountNotRecognised extends FailureCommonElements {
  faultCode: '106',
  faultText: 'Account not recognised'
}

interface Failure_MessageNotDeliveredWillRetry extends FailureCommonElements {
  faultCode: '105',
  faultText: 'The message has not been delivered to the destination but will be retried.'
}

interface Failure_InvalidServiceType extends FailureCommonElements {
  faultCode: '106',
  faultText: 'Invalid service type'
}

interface Failure_InvalidSwitchingAction extends FailureCommonElements {
  faultCode: '107',
  faultText: 'Invalid switching action'
}

export interface ResidentialSwitchMatchFailure {
  residentialSwitchMatchFailure:
    Failure_AddressNotFound |
    Failure_MissingOrIncompleteAddress |
    Failure_NameNotProvided |
    Failure_AccountNotRecognised |
    Failure_MessageNotDeliveredWillRetry |
    Failure_InvalidServiceType |
    Failure_InvalidSwitchingAction;
}