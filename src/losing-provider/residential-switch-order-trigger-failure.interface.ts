import { SwitchMessage } from "../common/switch-message.interface";

interface FailureCommonElements {
  faultElement?: string;
  faultElementValue?: string;
}

interface Failure_InvalidSwitchOrderReference extends FailureCommonElements {
  faultCode: '401',
  faultText: 'Invalid or missing switch order reference'
}

interface Failure_SwitchOrderReferenceUnavailable extends FailureCommonElements {
  faultCode: '402',
  faultText: 'Switch order reference is no longer available'
}

interface Failure_InvalidSwitchDate extends FailureCommonElements {
  faultCode: '403',
  faultText: 'Invalid or missing planned switch date'
}

export interface ResidentialSwitchOrderTriggerFailure {
  residentialSwitchOrderTriggerFailure:
    Failure_InvalidSwitchOrderReference |
    Failure_SwitchOrderReferenceUnavailable |
    Failure_InvalidSwitchDate;
}