import { SwitchMessage } from "../common/switch-message.interface";

interface FailureCommonElements {
  faultElement?: string;
  faultElementValue?: string;
}

interface Failure_InvalidSwitchOrderReference extends FailureCommonElements {
  faultCode: '201',
  faultText: 'Invalid or missing switch order reference'
}

interface Failure_SwitchOrderReferenceUnavailable extends FailureCommonElements {
  faultCode: '202',
  faultText: 'Switch order reference is no longer available'
}

interface Failure_InvalidSwitchDate extends FailureCommonElements {
  faultCode: '203',
  faultText: 'Invalid or missing planned switch date'
}

export interface ResidentialSwitchOrderFailure {
  residentialSwitchOrderFailure:
    Failure_InvalidSwitchOrderReference |
    Failure_SwitchOrderReferenceUnavailable |
    Failure_InvalidSwitchDate;
}