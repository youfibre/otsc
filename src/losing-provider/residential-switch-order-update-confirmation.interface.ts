import { SwitchMessage } from "../common/switch-message.interface";

export interface ResidentialSwitchOrderUpdateConfirmation {
  residentialSwitchOrderUpdateConfirmation: {
    status: 'updated'
  }
}