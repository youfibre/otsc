import { SwitchMessage } from "../common/switch-message.interface";

interface FailureCommonElements {
  faultElement?: string;
  faultElementValue?: string;
}

interface Failure_InvalidSwitchOrderReference extends FailureCommonElements {
  faultCode: '301',
  faultText: 'Invalid or missing switch order reference'
}

interface Failure_SwitchOrderReferenceUnavailable extends FailureCommonElements {
  faultCode: '302',
  faultText: 'Switch order reference is no longer available'
}

interface Failure_InvalidSwitchDate extends FailureCommonElements {
  faultCode: '303',
  faultText: 'Invalid or missing planned switch date'
}

export interface ResidentialSwitchOrderUpdateFailure {
  residentialSwitchOrderUpdateFailure:
    Failure_InvalidSwitchOrderReference |
    Failure_SwitchOrderReferenceUnavailable |
    Failure_InvalidSwitchDate;
}