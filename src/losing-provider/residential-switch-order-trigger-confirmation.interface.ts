import { SwitchMessage } from "../common/switch-message.interface";

export interface ResidentialSwitchOrderTriggerConfirmation {
  residentialSwitchOrderTriggerConfirmation: {
    status: 'triggered'
  }
}