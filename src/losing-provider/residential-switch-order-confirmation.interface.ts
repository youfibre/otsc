import { SwitchMessage } from "../common/switch-message.interface";

export interface ResidentialSwitchOrderConfirmation {
  residentialSwitchOrderConfirmation: {
    status: 'confirmed'
  }
}