import { SwitchMessage } from "../common/switch-message.interface";

export interface ResidentialSwitchOrderCancellationConfirmation {
  residentialSwitchOrderCancellationConfirmation: {
    status: 'cancelled'
  }
}