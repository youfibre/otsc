import { SwitchMessage } from "../common/switch-message.interface";

export interface MatchingSwitchOrder {
  switchOrderReference: string; // uuid
  matchType: 'consumer';
  services: ({
    serviceType: 'IAS';
    switchAction: 'ServiceFound' | 'ServiceWithAnotherCust' | 'ServiceWithAnotherRCP' | 'ServiceNotFound' | 'ForcedCease' | 'OptionToCease' | 'OptionToRetain';
    ACPID?: string;
    serviceIdentifiers?: {
      identifierType: 'NetworkOperator' | 'DN' | 'PartialDN',
      identifier: string;
    }[]
  } | {
    serviceType: 'NBICS';
    switchAction: 'ServiceFound' | 'ServiceWithAnotherCust' | 'ServiceWithAnotherRCP' | 'ServiceNotFound' | 'ForcedCease' | 'OptionToCease' | 'OptionToRetain';
    serviceIdentifiers?: {
      identifierType: 'DN' | 'PartialDN',
      identifier: string;
    }[]
  })[]
}

export interface SentImplication {
  sentMethod: string;
  sentTo?: string;
  sentBy: string;
}

export interface ResidentialSwitchMatchConfirmation {
  residentialSwitchMatchConfirmation: {
    implicationsSent: SentImplication[];
    matchResult: MatchingSwitchOrder;
    alternativeSwitchOrders?: {
      matchResult: MatchingSwitchOrder
    }[];
  }
}