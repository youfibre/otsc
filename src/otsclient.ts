import { SwitchMessage } from "./common/switch-message.interface";
import axios, { AxiosInstance } from "axios";

interface OAuthAccessTokenResponse {
  access_token: string;
  token_type: string;
  scope: string;
  expires_in: number;
}

interface AccessToken extends OAuthAccessTokenResponse {
  expires_at: Date;
}

type PostMessageResponse = {
  success: boolean;
  body: object;
};

type DirectoryResponse = {
  code: '400',
  message: 'Bad Request',
  description: string;
} | {
  code: '900901',
  message: 'Invalid Credentials',
  description: string;
} | {
  list: {
    listType: 'RCPID',
    identity: {
      id: string;
      name: string;
      processSupport?: {
        process: string;
        status: 'Active' | 'Suspend';
      }[],
      resource?: {
        name: 'customerAssistURL' | 'salesAssistURL' | string;
        type: 'URL' | string;
        value: string;
      }[]
    }[]
  }[]
};

interface CacheOptions {
  getItem(key: string): Promise<string | null>;
  setItem(key: string, value: string, ttlSeconds: number): void;
}

export class OTSClient {
  private apiClient: AxiosInstance;
  private sitClient: AxiosInstance;
  private accessToken: AccessToken | undefined;
  
  constructor(
    apiUrl: string,
    sitUrl: string,
    private readonly otsClientId: string,
    private readonly otsClientSecret: string,
    private readonly cacheOptions?: CacheOptions,
    private readonly testing: boolean = false,
  ) {
    this.apiClient = axios.create({
      baseURL: apiUrl,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    });

    this.sitClient = axios.create({
      baseURL: sitUrl,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    });
  }

  private async getAccessToken() {
    const fromCache = await this.cacheOptions?.getItem('otsc-accessToken');
    if (fromCache) {
      this.accessToken = JSON.parse(fromCache);
    }

    if (this.accessToken && this.accessToken.expires_at > new Date()) {
      return this.accessToken.access_token;
    }
    
    const response = await this.sitClient.post<OAuthAccessTokenResponse>('/oauth2/token', {
      grant_type: 'client_credentials',
      client_id: this.otsClientId,
      client_secret: this.otsClientSecret,
    }, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      }
    });

    this.accessToken = {
      ...response.data,
      expires_at: new Date(Date.now() + response.data.expires_in * 1000),
    };
    this.cacheOptions?.setItem(
      'otsc-accessToken',
      JSON.stringify(this.accessToken),
      this.accessToken.expires_in-60, // Deduct one minute just to be safe
    );

    console.log(this.accessToken);

    return response.data.access_token;
  }

  public async postMessage(switchMessage: SwitchMessage): Promise<PostMessageResponse> {
    const accessToken = await this.getAccessToken();

    try {
      const response = await this.apiClient.post(`/${this.testing ? 'testharness' : 'letterbox'}/v1/post`, switchMessage, {
        headers: {
          'Authorization': `Bearer ${accessToken}`,
          'Content-Type': 'application/json',
        },
      });
      return {
        success: true,
        body: response.data,
      };
    } catch (error) {
      if (error.isAxiosError) {
        return {
          success: false,
          body: error.response?.data,
        };
      }
      throw error;
    }
  }

  public async getDirectory(): Promise<DirectoryResponse> {
    const fromCache = await this.cacheOptions?.getItem(`otsc-directory`);
    if (fromCache) {
      return JSON.parse(fromCache);
    }

    const accessToken = await this.getAccessToken();
    const response = await this.apiClient.get<DirectoryResponse>(`/directory/v1/entry?listType=RCPID&identity=all`, {
      headers: {
        'Authorization': `Bearer ${accessToken}`,
        'Content-Type': 'application/json',
      },
    });

    const secondsUntilMidnight = (24*60*60) - (Date.now() % (24*60*60*1000));
    this.cacheOptions?.setItem(
      `otsc-directory`,
      JSON.stringify(response.data),
      secondsUntilMidnight,
    );
    return response.data;
  }
}
