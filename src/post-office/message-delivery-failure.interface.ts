export interface MessageDeliveryFailure {
  messageDeliveryFailure: {
    code: '9005';
    text: 'Unable to deliver the message to the destination, no valid route.';
    severity: 'Failure';
  } | {
    code: '9006';
    text: 'Unable to deliver the message to the destination, reject, invalid message format.';
    severity: 'Failure';
  } | {
    code: '9007';
    text: 'Recipient rejected message.';
    severity: 'Failure';
  } | {
    code: '9008';
    text: 'Unable to deliver the message to the destination, timed out.';
    severity: 'Failure';
  } | {
    code: '9013';
    text: 'Unable to deliver the message to the destination - Invalid API Key';
    severity: 'Failure';
  } | {
    code: '9014';
    text: 'Unable to deliver the message to the destination - API Key expired';
    severity: 'Failure';
  } | {
    code: '9015';
    text: 'Unable to deliver the message to the destination - Digital certificate invalid';
    severity: 'Failure';
  } | {
    code: '9016';
    text: 'Unable to deliver the message to the destination - Digital certificate expired';
    severity: 'Failure';
  };
}