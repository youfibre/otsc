export enum MessageType {
  GP_RESIDENTIAL_SWITCH_MATCH_REQUEST = 'residentialSwitchMatchRequest',
  GP_RESIDENTIAL_SWITCH_ORDER_REQUEST = 'residentialSwitchOrderRequest',
  GP_RESIDENTIAL_SWITCH_ORDER_UPDATE_REQUEST = 'residentialSwitchOrderUpdateRequest',
  GP_RESIDENTIAL_SWITCH_ORDER_TRIGGER_REQUEST = 'residentialSwitchOrderTriggerRequest',
  GP_RESIDENTIAL_SWITCH_ORDER_CANCELLATION_REQUEST = 'residentialSwitchOrderCancellationRequest',

  LP_RESIDENTIAL_SWITCH_MATCH_FAILURE = 'residentialSwitchMatchFailure',
  LP_RESIDENTIAL_SWITCH_MATCH_CONFIRMATION = 'residentialSwitchMatchConfirmation',
  LP_RESIDENTIAL_SWITCH_ORDER_FAILURE = 'residentialSwitchOrderFailure',
  LP_RESIDENTIAL_SWITCH_ORDER_CONFIRMATION = 'residentialSwitchOrderConfirmation',
  LP_RESIDENTIAL_SWITCH_ORDER_UPDATE_FAILURE = 'residentialSwitchOrderUpdateFailure',
  LP_RESIDENTIAL_SWITCH_ORDER_UPDATE_CONFIRMATION = 'residentialSwitchOrderUpdateConfirmation',
  LP_RESIDENTIAL_SWITCH_ORDER_TRIGGER_FAILURE = 'residentialSwitchOrderTriggerFailure',
  LP_RESIDENTIAL_SWITCH_ORDER_TRIGGER_CONFIRMATION = 'residentialSwitchOrderTriggerConfirmation',
  LP_RESIDENTIAL_SWITCH_ORDER_CANCELLATION_FAILURE = 'residentialSwitchOrderCancellationFailure',
  LP_RESIDENTIAL_SWITCH_ORDER_CANCELLATION_CONFIRMATION = 'residentialSwitchOrderCancellationConfirmation',

  PO_MESSAGE_DELIVERY_FAILURE = 'messageDeliveryFailure',
}