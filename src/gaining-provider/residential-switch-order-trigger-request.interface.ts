import { SwitchMessage } from "../common/switch-message.interface";

export interface ResidentialSwitchOrderTriggerRequest {
  residentialSwitchOrderTriggerRequest: {
    switchOrderReference: string
  }
}