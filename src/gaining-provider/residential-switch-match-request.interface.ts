import { Address } from "../common/address.interface";

export interface ResidentialSwitchMatchRequest {
  residentialSwitchMatchRequest: {
    grcpBrandName: string;
    name: string;
    account?: string;
    address: Address;
    services: ({
      serviceType: 'IAS';
      action: 'cease';
    } | {
      serviceType: 'NBICS';
      serviceIdentifierType?: 'DN';
      serviceIdentifier?: string;
      action: 'cease' | 'port'
    })[];
  }
}