import { SwitchMessage } from "../common/switch-message.interface";

export interface ResidentialSwitchOrderCancellationRequest {
  residentialSwitchOrderCancellationRequest: {
    switchOrderReference: string;
  }
}