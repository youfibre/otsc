import { SwitchMessage } from "../common/switch-message.interface";

export interface ResidentialSwitchOrderUpdateRequest {
  residentialSwitchOrderUpdateRequest: {
    switchOrderReference: string,
    plannedSwitchDate: string
  }
}