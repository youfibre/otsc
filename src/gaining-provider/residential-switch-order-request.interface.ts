import { SwitchMessage } from "../common/switch-message.interface";

export interface ResidentialSwitchOrderRequest {
  residentialSwitchOrderRequest: {
    switchOrderReference: string,
    plannedSwitchDate: string
  }
}